#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlus8.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlus8-user \
    lineage_OnePlus8-userdebug \
    lineage_OnePlus8-eng
